cmake_minimum_required(VERSION 3.18)

project(Alpela VERSION 7)

set(FREEDESKTOP_APP_ID io.gitlab.z_s_e.alpela)

find_package(Qt6 REQUIRED COMPONENTS Widgets DBus)
qt_standard_project_setup()
set(CMAKE_AUTORCC ON)

find_package(PkgConfig REQUIRED)
pkg_check_modules(libavformat REQUIRED IMPORTED_TARGET libavformat)
pkg_check_modules(libavcodec REQUIRED IMPORTED_TARGET libavcodec)
pkg_check_modules(libavutil REQUIRED IMPORTED_TARGET libavutil)

if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/deps/lbu/CMakeLists.txt)
    message(FATAL_ERROR "Use 'git submodule update --init' to make bundled deps available")
endif()
set(LBU_BUILD_STATIC ON CACHE BOOL "link lbu library statically")
set(LBU_BUILD_TESTS OFF CACHE BOOL "no lbu tests needed")
add_subdirectory(deps/lbu EXCLUDE_FROM_ALL)
set(ALPE_BUILD_PIPEWIRE ON CACHE BOOL "enable PipeWire support")
add_subdirectory(deps/alpe EXCLUDE_FROM_ALL)

add_library(ffmpeg_wrapper STATIC src/ffmpeg_wrapper.c)
target_compile_features(ffmpeg_wrapper PRIVATE c_std_11)
target_link_libraries(ffmpeg_wrapper PRIVATE PkgConfig::libavformat PkgConfig::libavcodec PkgConfig::libavutil)

set(PROJECT_SOURCES
    src/alpeinterface.cpp
    src/alpeinterface.h
    src/debuglog.cpp
    src/debuglog.h
    src/ffmpeg_wrapper.h
    src/ffmpegthread.cpp
    src/ffmpegthread.h
    src/main.cpp
    src/mainwindow.cpp
    src/mainwindow.h
    src/mainwindow.ui
    src/seekbar.cpp
    src/seekbar.h
    src/singleinstance.cpp
    src/singleinstance.h
    src/tcp_stream_with_timeout.cpp
    src/tcp_stream_with_timeout.h
    src/waveformpreview.h
    src/waveformpreviewgenerator.cpp
    src/waveformpreviewgenerator.h
    res/resources.qrc
)
qt_add_executable(${PROJECT_NAME} ${PROJECT_SOURCES})

target_link_libraries(${PROJECT_NAME} PRIVATE
    Qt6::Widgets Qt6::DBus
    alpe::alpe
    ffmpeg_wrapper
)
target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)
target_compile_definitions(${PROJECT_NAME} PRIVATE PROJECT_NAME="${PROJECT_NAME}" PROJECT_VERSION="${PROJECT_VERSION}" FREEDESKTOP_APP_ID="${FREEDESKTOP_APP_ID}")

set(WARNING_FLAGS "-pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wold-style-cast -Wredundant-decls -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wundef -Wno-unused -Wno-type-limits")
if(CMAKE_COMPILER_IS_GNUCXX)
    set(WARNING_FLAGS "${WARNING_FLAGS} -Wstrict-null-sentinel -Wlogical-op")
endif()
separate_arguments(WARNING_FLAGS)
target_compile_options(${PROJECT_NAME} PRIVATE ${WARNING_FLAGS})


include(GNUInstallDirs)
install(TARGETS ${PROJECT_NAME})
install(FILES res/${FREEDESKTOP_APP_ID}.desktop DESTINATION ${CMAKE_INSTALL_DATADIR}/applications)
set(ICON_INSTALL_BASE ${CMAKE_INSTALL_DATADIR}/icons/hicolor)
install(FILES res/icon.png RENAME ${FREEDESKTOP_APP_ID}.png DESTINATION ${ICON_INSTALL_BASE}/48x48/apps)
install(FILES res/icon.svg RENAME ${FREEDESKTOP_APP_ID}.svg DESTINATION ${ICON_INSTALL_BASE}/scalable/apps)
