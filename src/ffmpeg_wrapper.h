/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FFMPEG_WRAPPER_H
#define FFMPEG_WRAPPER_H

#include <alsa/asoundlib.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ffmpeg_wrapper;

struct ffmpeg_wrapper* ffmpeg_wrapper_create(const char* path);
void ffmpeg_wrapper_free(struct ffmpeg_wrapper* wrapper);

typedef struct {
    snd_pcm_format_t type;
    int rate;
    int channels;
    int is_planar;
    int64_t length;
} ffmpeg_wrapper_file_info;

ffmpeg_wrapper_file_info ffmpeg_wrapper_get_file_info(struct ffmpeg_wrapper* wrapper);

typedef void (*push_frames_func)(const uint8_t* const * sample_data, unsigned frame_offset, unsigned frame_count, void* user_data);
int ffmpeg_wrapper_get_frames(struct ffmpeg_wrapper* wrapper, int frame_count, push_frames_func psf, void* user_data);

int ffmpeg_wrapper_seek(struct ffmpeg_wrapper* wrapper, int64_t pos);

#ifdef __cplusplus
}
#endif

#endif
