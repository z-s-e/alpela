/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "alpeinterface.h"
#include "waveformpreviewgenerator.h"

#include <algorithm>
#include <lbu/math.h>
#include <QApplication>
#include <QDebug>
#include <QDropEvent>
#include <QFileDialog>
#include <QKeyEvent>
#include <QMenu>
#include <QMetaObject>
#include <QMimeData>
#include <QSettings>
#include <QSocketNotifier>
#include <QThread>
#include <QTimer>
#include <QUrl>


static const char settingsKeyOutput[] = "output";
static const char settingsKeyOverrideParams[] = "overrideParams";
static const char settingsKeyBufferTime[] = "bufferTime";
static const char settingsKeyPreUnderrunTime[] = "preUnderrunTime";
static const char settingsKeyRewindSafeguard[] = "rewindSafeguard";
static const char settingsKeyEmergencyPauseFade[] = "emergencyPauseFade";
static const char settingsKeyDefaultFadeType[] = "defaultFadeType";
static const char settingsKeyDefaultFadeDuration[] = "defaultFadeDuration";
static const char settingsKeyFades[] = "fades";


static QString singleUrlFromUriList(const QMimeData& data)
{
    static const char UriList[] = "text/uri-list";
    auto list = QString::fromLocal8Bit(data.data(UriList)).split("\r\n", Qt::SkipEmptyParts);
    list.erase(std::remove_if(list.begin(), list.end(), [](const QString& s) { return s.startsWith('#'); }), list.end());
    return list.size() == 1 ? list[0] : QString();
}

static QString checkAcceptDrop(QDropEvent* event)
{
    const auto url = singleUrlFromUriList(*event->mimeData());
    if( ! url.isEmpty() && (event->possibleActions() & Qt::CopyAction) ) {
        event->setDropAction(Qt::CopyAction);
        event->accept();
        return url;
    } else {
        event->ignore();
        return {};
    }
}

static QStringList fadeTypes() { return {"Linear", "Moderate", "Steep"}; }

static alpe::fade_parameter::Type fadeTypeFromString(const QString& type)
{
    if( type == "Moderate" ) return alpe::fade_parameter::Type::Moderate;
    if( type == "Steep" ) return alpe::fade_parameter::Type::Steep;
    return alpe::fade_parameter::Type::Linear;
}

static QString fadeTypeToString(alpe::fade_parameter::Type type)
{
    return fadeTypes()[static_cast<int>(type)];
}

static alpe::short_duration_frames fadeTimeToFrames(std::chrono::milliseconds t, unsigned sampleRate)
{
    return sampleRate == 0 ? alpe::short_duration_frames(0) : alpe::frames_for_time(t, sampleRate);
}

static bool isPendingOrResetting(alpe_thread::State s)
{
    return s == alpe_thread::State::CommandPending || s == alpe_thread::State::Resetting;
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , fadesMenu(new QMenu(this))
    , updateTimer(new QTimer(this))
{
    ui->setupUi(this);
    setFocus();

    labelState = new QLabel(this);
    statusBar()->addWidget(labelState);
    labelTime = new QLabel(ui->seekBar->createTimeLabel(), this);
    statusBar()->addPermanentWidget(labelTime);

    ui->toolFade->setMenu(fadesMenu);

    interface.reset(new AlpeInterface(this));
    alpeThread.reset(new alpe_thread(interface.get(), interface.get()));

    {
        auto notifier = new QSocketNotifier(alpeThread->event_fd().fd, QSocketNotifier::Read, this);
        connect(notifier, &QSocketNotifier::activated, this, &MainWindow::updateState);
    }
    updateTimer->setSingleShot(true);
    connect(updateTimer, &QTimer::timeout, this, &MainWindow::updateState);

    connect(ui->buttonPlayPause, &QPushButton::clicked, this, &MainWindow::togglePlayPause);
    connect(ui->buttonStop, &QPushButton::clicked, this, &MainWindow::stop);
    connect(ui->buttonRewindStart, &QPushButton::clicked, this, &MainWindow::rewindStart);
    connect(ui->seekBar, &SeekBar::requestSeek, this, &MainWindow::seek);
    connect(ui->buttonWaveform, &QAbstractButton::toggled, this, &MainWindow::togglePreview);
    connect(ui->buttonFile, &QPushButton::clicked, this, [this] {
        const QString audioGlobs("*.aac *.ac3 *.adts *.aif *.aiff *.ass *.f4a *.flac *.m4a *.mka *.mp+ *.mp2 *.mp3 *.mpc *.mpga *.mpp *.oga *.ogg *.opus *.ra *.rax *.tta *.wav *.wma");
        auto f = QFileDialog::getOpenFileName(this, tr("Open audio file"), {},
                                              QString("%1 (%2);;%3 (*)").arg(tr("Audio files"), audioGlobs, tr("No filter")));
        if( ! f.isEmpty() )
            openFile(f);
    });

    connect(ui->buttonClearFades, &QPushButton::clicked, this, [this] { settingsFadesTmp.clear(); });
    connect(ui->buttonAddFade, &QPushButton::clicked, this, [this] {
        settingsFadesTmp.append(QString("%1;%2;%3").arg(ui->comboFadeDir->currentText(),
                                                        ui->comboFadeType->currentText(),
                                                        ui->spinFadeDuration->cleanText()));
    });

    ui->comboDefaultFadeType->addItems(fadeTypes());
    ui->comboFadeType->addItems(fadeTypes());
    connect(ui->buttonSettings, &QPushButton::clicked, this, &MainWindow::openSettings);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &MainWindow::saveSettings);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, [this] { ui->stackedWidget->setCurrentIndex(0); });

    ui->labelVersion->setText(qApp->applicationVersion());

    connect(ui->debugLog, &DebugLog::requestIODelay, this, [this] { interface->injectIODelay(); });

    loadSettings();
    updateState();
}

MainWindow::~MainWindow()
{
    while( isPendingOrResetting(alpeThread->state()) )
        QThread::msleep(50);
    interface->setFilePath({});
    alpeThread->reset(defaultFadeParam());
    alpeThread->wait_for_playback_finish();
    ui->buttonWaveform->setChecked(false);
}

void MainWindow::openFile(const QString& filePath)
{
    if( filePath.isEmpty() )
        return;

    if( isPendingOrResetting(lastState) ) {
        openPending = filePath;
        return;
    }

    if( previewGenerator == nullptr )
        ui->buttonWaveform->setEnabled(true);
    else
        ui->buttonWaveform->setChecked(false);

    debugAppend(QtInfoMsg, QString("Opening file %1 ...").arg(filePath));
    ui->labelFile->setText(filePath);
    interface->setFilePath(filePath.toStdString());
    alpeThread->reset_autoplay(defaultFadeParam());
    updateState();
}

void MainWindow::debugAppend(QtMsgType level, const QString& msg)
{
    char time[13] = {};
    alpe::debug_logger::print_unix_time_day_clock(lbu::array_ref<char>(time));

    QString levelStr;
    switch( level ) {
    case QtDebugMsg:
        levelStr = "Debug";
        break;
    case QtWarningMsg:
        levelStr = "WARN ";
        break;
    case QtCriticalMsg:
        levelStr = "ERROR";
        break;
    case QtFatalMsg:
        levelStr = "FATAL";
        break;
    case QtInfoMsg:
        levelStr = "Info ";
        break;
    }

    auto formatted = QString("%1 %2: %3").arg(QString(time), levelStr, msg);
    QMetaObject::invokeMethod(this, [this, formatted] {
            ui->debugLog->append(formatted);
        }, Qt::QueuedConnection);
}

void MainWindow::updateFileInfo(int64_t frameCount, unsigned int period, unsigned int rate)
{
    QMetaObject::invokeMethod(this, [this, frameCount, period, rate] {
            ui->seekBar->setFileData(frameCount, rate);
            interpolateLimit = period + period / 2;
        }, Qt::BlockingQueuedConnection);
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    const auto key = event->key();
    if( key == Qt::Key_Space ) {
        if( ! ui->buttonPlayPause->isEnabled() )
            return;
        togglePlayPause();
    } else if( key >= Qt::Key_0 && key <= Qt::Key_9 ) {
        const auto frames = ui->seekBar->frames();
        const bool seekEnabled = ui->seekBar->isEnabled() && frames > 0;
        if( ! seekEnabled || event->isAutoRepeat() )
            return;
        seek((frames / 10) * (key - Qt::Key_0));
    } else if( (key == Qt::Key_Left || key == Qt::Key_Right) ) {
        updateState(); // called to hopefully get more accurate current position
        const auto frames = ui->seekBar->frames();
        const bool seekEnabled = ui->seekBar->isEnabled() && frames > 0;
        if( ! seekEnabled )
            return;

        int64_t frameDelta = ui->seekBar->rate();
        switch( event->modifiers() ) {
        case (Qt::ShiftModifier | Qt::ControlModifier):
            frameDelta *= 30;
            break;
        case Qt::ShiftModifier:
            frameDelta *= 10;
            break;
        default:
            frameDelta *= 2;
        }
        if( key == Qt::Key_Left )
            frameDelta *= -1;

        seek(qBound(int64_t(0), ui->seekBar->position() + frameDelta, frames));
    } else {
        QMainWindow::keyPressEvent(event);
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event) { checkAcceptDrop(event); }
void MainWindow::dragMoveEvent(QDragMoveEvent* event) { checkAcceptDrop(event); }
void MainWindow::dragLeaveEvent(QDragLeaveEvent*) {}
void MainWindow::dropEvent(QDropEvent* event) { openFile(QUrl(checkAcceptDrop(event)).toLocalFile()); }

alpe::fade_parameter MainWindow::defaultFadeParam() const
{
    return {defaultFade.type, fadeTimeToFrames(defaultFade.duration, ui->seekBar->rate())};
}

void MainWindow::loadFadesTmp(QSettings& s)
{
    if( s.contains(settingsKeyFades) )
        settingsFadesTmp = s.value(settingsKeyFades).toString().split('#', Qt::SkipEmptyParts);
    else
        settingsFadesTmp = QStringList({"IN;Linear;2000", "IN;Moderate;2000", "OUT;Linear;5000", "OUT;Moderate;5000", "OUT;Steep;5000"});
}

void MainWindow::loadSettings()
{
    QSettings settings;
    {
        AlpeInterface::Settings s{};
        s.output = settings.value(settingsKeyOutput).toString().toStdString();
        s.overrideParams = settings.value(settingsKeyOverrideParams, s.overrideParams).toBool();
        s.bufferTime = settings.value(settingsKeyBufferTime, s.bufferTime).toInt();
        s.preUnderrunTime = settings.value(settingsKeyPreUnderrunTime, s.preUnderrunTime).toInt();
        s.rewindSafeguard = settings.value(settingsKeyRewindSafeguard, s.rewindSafeguard).toInt();
        s.emergencyPauseFade = settings.value(settingsKeyEmergencyPauseFade, s.emergencyPauseFade).toInt();
        interface->setSettings(s);
    }
    {
        DefaultFade f{};
        f.type = fadeTypeFromString(settings.value(settingsKeyDefaultFadeType, fadeTypeToString(f.type)).toString());
        f.duration = std::chrono::milliseconds(settings.value(settingsKeyDefaultFadeDuration, int(f.duration.count())).toInt());
        defaultFade = f;
    }
    fadesMenu->clear();
    loadFadesTmp(settings);
    for( const auto& f : settingsFadesTmp )
        fadesMenu->addAction(f, this, [this, f] { fade(f); });
}

void MainWindow::openSettings()
{
    const auto currentInterfaceSettings = interface->settings();
    ui->lineOutput->setText(QString::fromStdString(currentInterfaceSettings.output));
    ui->checkParamOverride->setChecked(currentInterfaceSettings.overrideParams);
    ui->spinBufferTime->setValue(currentInterfaceSettings.bufferTime);
    ui->spinPreUnderrun->setValue(currentInterfaceSettings.preUnderrunTime);
    ui->spinRewindSafeguard->setValue(currentInterfaceSettings.rewindSafeguard);
    ui->spinEmergencyPause->setValue(currentInterfaceSettings.emergencyPauseFade);
    ui->comboDefaultFadeType->setCurrentIndex(static_cast<int>(defaultFade.type));
    ui->spinDefaultFadeDuration->setValue(defaultFade.duration.count());
    QSettings settings;
    loadFadesTmp(settings);
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::saveSettings()
{
    if( isPendingOrResetting(lastState) )
        return;

    {
        QSettings settings;
        settings.setValue(settingsKeyOutput, ui->lineOutput->text());
        settings.setValue(settingsKeyOverrideParams, ui->checkParamOverride->isChecked());
        settings.setValue(settingsKeyBufferTime, ui->spinBufferTime->value());
        settings.setValue(settingsKeyPreUnderrunTime, ui->spinPreUnderrun->value());
        settings.setValue(settingsKeyRewindSafeguard, ui->spinRewindSafeguard->value());
        settings.setValue(settingsKeyEmergencyPauseFade, ui->spinEmergencyPause->value());
        settings.setValue(settingsKeyDefaultFadeType, ui->comboDefaultFadeType->currentText());
        settings.setValue(settingsKeyDefaultFadeDuration, ui->spinDefaultFadeDuration->value());
        settings.setValue(settingsKeyFades, settingsFadesTmp.join('#'));
    }

    loadSettings();
    ui->stackedWidget->setCurrentIndex(0);
    if( ! interface->filePath().empty() )
        alpeThread->reset_autoplay(defaultFadeParam());
}

void MainWindow::togglePlayPause()
{
    if( lastState == alpe_thread::State::CommandPending )
        return;

    switch( alpeThread->state() ) {
    case alpe_thread::State::Initial:
    case alpe_thread::State::Paused:
        if( alpeThread->last_playback_position().position == 0 )
            alpeThread->play({});
        else
            alpeThread->play(defaultFadeParam());
        break;
    default:
        alpeThread->pause(defaultFadeParam());
    }
    updateState();
}

void MainWindow::stop()
{
    if( lastState == alpe_thread::State::CommandPending )
        return;
    alpeThread->stop(defaultFadeParam());
    updateState();
}

void MainWindow::rewindStart()
{
    if( lastState == alpe_thread::State::CommandPending )
        return;
    alpeThread->seek_autoplay(0, defaultFadeParam(), alpe::fade_parameter());
    updateState();
}

void MainWindow::fade(const QString& fadeString)
{
    if( lastState == alpe_thread::State::CommandPending )
        return;

    const auto tmp = fadeString.split(';');
    if( tmp.size() != 3 ) {
        qWarning() << "Bad fade string" << fadeString;
        return;
    }

    const auto type = fadeTypeFromString(tmp[1]);
    const auto frames = fadeTimeToFrames(std::chrono::milliseconds(tmp[2].toInt()), ui->seekBar->rate());
    if( tmp[0] == "IN" )
        alpeThread->play({type, frames});
    else
        alpeThread->pause({type, frames});
}

void MainWindow::seek(int64_t pos)
{
    if( lastState == alpe_thread::State::CommandPending )
        return;
    const auto f = defaultFadeParam();
    alpeThread->seek(uint64_t(pos), f, pos == 0 ? alpe::fade_parameter() : f);
    updateState();
}

void MainWindow::openFileIfPending()
{
    if( openPending.isEmpty() )
        return;
    const auto f = openPending;
    openPending.clear();
    openFile(f);
}

void MainWindow::updateState()
{
    if( lastState == alpe_thread::State::CommandPending )
        unsetCursor();

    bool interpolatePosition = false;
    bool resetting = false;

    lastState = alpeThread->state();
    switch( lastState ) {
    case alpe_thread::State::Initial:
        labelState->setText(tr("Idle"));
        ui->buttonPlayPause->setEnabled( ! interface->filePath().empty());
        ui->buttonStop->setEnabled(false);
        ui->buttonRewindStart->setEnabled(false);
        ui->toolFade->setEnabled(false);
        ui->seekBar->setFileData(-1, 0);
        labelTime->setText(ui->seekBar->createTimeLabel());
        openFileIfPending();
        return;
    case alpe_thread::State::CommandPending:
        setCursor(Qt::WaitCursor);
        return;
    case alpe_thread::State::Paused:
        labelState->setText(tr("Paused"));
        break;
    case alpe_thread::State::Buffering:
        labelState->setText(tr("Buffering"));
        break;
    case alpe_thread::State::Playing:
        labelState->setText(tr("Playing"));
        interpolatePosition = true;
        break;
    case alpe_thread::State::Seeking:
        labelState->setText(tr("Seeking"));
        break;
    case alpe_thread::State::FadingOut:
        labelState->setText(tr("Fading out"));
        interpolatePosition = true;
        break;
    case alpe_thread::State::Resetting:
        labelState->setText(tr("Resetting"));
        resetting = true;
        break;
    }

    ui->buttonPlayPause->setEnabled( ! resetting);
    ui->buttonStop->setEnabled( ! resetting);
    ui->buttonRewindStart->setEnabled( ! resetting);
    ui->toolFade->setEnabled( ! resetting && ! fadesMenu->isEmpty());
    ui->seekBar->setEnabled( ! resetting);

    const auto lastPos = alpeThread->last_playback_position();
    auto currentPos = int64_t(lastPos.position);
    if( interpolatePosition ) {
        const auto sampleRate = ui->seekBar->rate();
        Q_ASSERT(sampleRate > 0);
        currentPos += alpe::frames_for_time(std::chrono::steady_clock::now() - lastPos.time, sampleRate);

        const auto nextSecondPos = lbu::idiv_ceil(currentPos + 1, int64_t(sampleRate)) * sampleRate;
        const auto timeToNextSecond = alpe::time_for_frames(nextSecondPos - currentPos, sampleRate);
        updateTimer->start(std::chrono::ceil<std::chrono::milliseconds>(timeToNextSecond) + std::chrono::milliseconds(10));

        currentPos = qMin(currentPos, int64_t(lastPos.position + interpolateLimit)); // do not make too large corrections
    }
    ui->seekBar->setFramePosition(currentPos);
    labelTime->setText(ui->seekBar->createTimeLabel());

    openFileIfPending();
}

void MainWindow::togglePreview()
{
    if( ! ui->buttonWaveform->isChecked() ) {
        ui->seekBar->setWaveformPreview(nullptr, 0);
        if( previewPending ) {
            previewGenerator->cancelPreview();
            ui->buttonWaveform->setEnabled(false);
            ui->buttonPlayPause->setFocus();
        }
        return;
    }

    if( previewGenerator == nullptr ) {
        previewGenerator = new WaveformPreviewGenerator(this);
        connect(previewGenerator, &WaveformPreviewGenerator::progress, this, [this] (const QString& file,
                                                                                     unsigned endSample) {
            if( file == ui->labelFile->text() && ui->buttonWaveform->isChecked() )
                ui->seekBar->setWaveformPreview(previewGenerator->data(), endSample);
        });
        connect(previewGenerator, &WaveformPreviewGenerator::finished, this, [this] (const QString& file,
                                                                                     WaveformPreviewGenerator::Result result) {
            previewPending = false;
            ui->buttonWaveform->setEnabled(true);
            if( result == WaveformPreviewGenerator::ResultError ) {
                ui->buttonWaveform->setChecked(false);
                debugAppend(QtCriticalMsg, QString("Failed to generate waveform preview for '%1'").arg(file));
            }
        });
    }

    previewGenerator->requestPreview(ui->labelFile->text());
    previewPending = true;
}
