/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "alpeinterface.h"

#include "ffmpegthread.h"
#include "mainwindow.h"
#include "tcp_stream_with_timeout.h"

#include <alpe_pipewire.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <lbu/math.h>
#include <regex>


namespace {
struct SharedData {
    // shared between the main thread and the alpe_thread;
    // written by main thread while not resetting, read by alpe_thread while resetting
    std::string filePath;
    AlpeInterface::Settings settings;
};
}

struct AlpeInterface::Data {
    MainWindow* win = {};

    SharedData shared;

    // all following members should only be accessed from the callbacks that are called in the alpe_thread
    FfmpegThread ffmpegThread;

    alpe_pipewire_lib_wrapper pipewireWrapper;
    lbu::alsa::pcm_device alsa;
    tcp_stream_with_timeout tcp;

    std::unique_ptr<alpe> instance;
    std::string previousOutput;

    const std::regex IpPortRegex = std::regex("(\\d+\\.\\d+\\.\\d+\\.\\d+):(\\d+)");


    Data(MainWindow* window) : win(window) {}


    bool openInstanceAlsa(const char* dev)
    {
        if( int err = alsa.open(dev, SND_PCM_STREAM_PLAYBACK, lbu::alsa::pcm_device::FlagsNoAutoConversion); err < 0 ) {
            win->debugAppend(QtCriticalMsg, QString("error opening Alsa device %1: %2").arg(QString(dev), QString(snd_strerror(err))));
            return false;
        }
        win->debugAppend(QtInfoMsg, QString("Alsa device '%1' opened").arg(QString(dev)));
        instance = alpe::create_alsa(alsa);
        return true;
    }

    bool openInstancePipeWire()
    {
        const char* app_id = FREEDESKTOP_APP_ID;
        instance = alpe_create_pipewire(PROJECT_NAME, app_id, app_id);
        if( instance->state() == alpe::State::Error ) {
            win->debugAppend(QtWarningMsg, QString("PipeWire not available"));
            return false;
        }
        win->debugAppend(QtInfoMsg, QString("using PipeWire"));
        return true;
    }

    bool openInstanceTcp(const std::string& output, const std::smatch& match)
    {
        tcp.reset();

        struct sockaddr_in serv_addr = {};
        serv_addr.sin_family = AF_INET;
        const auto addr = match[1].str();
        if( inet_aton(addr.c_str(), &serv_addr.sin_addr) != 1 ) {
            win->debugAppend(QtCriticalMsg, QString("bad IP address: %1").arg(QString::fromStdString(addr)));
            return false;
        }
        serv_addr.sin_port = htons(std::atoi(match[2].str().c_str()));

        if( int err = tcp.connect(&serv_addr); err != 0 ) {
            win->debugAppend(QtCriticalMsg, QString("failed to connect to %1: %2").arg(QString::fromStdString(output),
                                                                                          QString(strerror(err))));
            return false;
        }
        win->debugAppend(QtInfoMsg, QString("connected to %1").arg(QString::fromStdString(output)));

        lbu::fd sock(tcp.descriptor());
        instance = alpe::create_stream(tcp.in(), lbu::poll::poll_fd(sock, lbu::poll::FlagsReadReady), tcp.out());
        return true;
    }

    bool openInstance()
    {
        const auto& output = shared.settings.output;

        if( instance && instance->state() == alpe::State::Ready && output == previousOutput )
            return true;

        instance.reset();
        alsa.close();
        previousOutput = output;

        std::smatch match;
        if( std::regex_match(output, match, IpPortRegex) )
            return openInstanceTcp(output, match);
        if( output.empty() && openInstancePipeWire() )
            return true;
        return openInstanceAlsa(output.empty() ? "default" : output.c_str());
    }
};

AlpeInterface::AlpeInterface(MainWindow* window) : d(new Data(window)) {}
AlpeInterface::~AlpeInterface() { delete d; }

void AlpeInterface::setFilePath(std::string path)
{
    d->shared.filePath = path;
}

void AlpeInterface::setSettings(Settings s)
{
    d->shared.settings = s;
}

std::string_view AlpeInterface::filePath()
{
    return d->shared.filePath;
}

AlpeInterface::Settings AlpeInterface::settings()
{
    return d->shared.settings;
}

void AlpeInterface::injectIODelay()
{
    d->ffmpegThread.injectLongDelay();
}

alpe_thread::interface::reset_data AlpeInterface::reset()
{
    if( d->shared.filePath.empty() ) {
        d->instance.reset();
        d->alsa.close();
        return {};
    }

    if( ! d->openInstance() )
        return {};

    d->instance->set_debug_logger(this);

    alpe::configuration cfg;

    if( ! d->ffmpegThread.open(d->shared.filePath.c_str()) ) {
        d->win->debugAppend(QtCriticalMsg, QString("cannot open %1").arg(d->shared.filePath.c_str()));
        return {};
    } else {
        const auto file_format = d->ffmpegThread.format();

        {
            auto c = d->instance->configure(file_format);
            if( std::holds_alternative<alpe::error_event>(c) ) {
                report_error(std::get<alpe::error_event>(c));
                return {};
            }
            cfg = std::get<alpe::configuration>(c);
        }

        if( cfg.format.rate != file_format.rate ) {
            d->win->debugAppend(QtCriticalMsg, "sample rate conversion not implemented");
            return {};
        }

        d->ffmpegThread.setDestinationType(cfg.format.type);

        if( d->shared.settings.overrideParams ) {
            alpe::engine_parameter p;
            using milliseconds = std::chrono::milliseconds;
            p.pre_underrun_trigger_frames = alpe::frames_for_time(milliseconds(d->shared.settings.preUnderrunTime), cfg.format.rate);
            p.rewind_safeguard = alpe::frames_for_time(milliseconds(d->shared.settings.rewindSafeguard), cfg.format.rate);
            p.emergency_pause_fade = alpe::frames_for_time(milliseconds(d->shared.settings.emergencyPauseFade), cfg.format.rate);

            const auto wanted = alpe::frames_for_time(milliseconds(d->shared.settings.bufferTime), cfg.format.rate);
            const auto hw = (cfg.period_buffer_count - 1) * cfg.period_frames;
            if( wanted > hw ) {
                p.additional_buffer_count = std::min<alpe::short_duration_frames>(lbu::idiv_ceil(wanted - hw, cfg.period_frames),
                                                                                  std::numeric_limits<uint8_t>::max());
            }

            auto c = d->instance->update_parameters(p);
            if( std::holds_alternative<alpe::error_event>(c) ) {
                report_error(std::get<alpe::error_event>(c));
                return {};
            }
            cfg = std::get<alpe::configuration>(c);
        }
    }

    d->win->updateFileInfo(d->ffmpegThread.frameCount(), cfg.period_frames, cfg.format.rate);

    reset_data result;
    result.instance = d->instance.get();
    result.buffer_ready_pollfd = d->ffmpegThread.bufferReadyPoll();
    result.post_underrun_fade_in = {alpe::fade_parameter::Type::Linear, cfg.parameter.emergency_pause_fade};
    return result;
}

void AlpeInterface::request_buffer(lbu::array_ref<void> buf) { d->ffmpegThread.requestBuffer(buf); }

alpe_thread::interface::buffer_ready_info AlpeInterface::buffer_ready()
{
    const auto info = d->ffmpegThread.requestResult();
    if( info.error )
        d->win->debugAppend(QtCriticalMsg, "ffmpeg read error");
    return {alpe::short_duration_frames(info.framesRead), info.eos ? alpe::EndOfStream::Yes : alpe::EndOfStream::No, info.error};
}

alpe::long_duration_frames AlpeInterface::seek(alpe::long_duration_frames pos)
{
    d->ffmpegThread.seek(int64_t(pos));
    return pos;
}

void AlpeInterface::report_underrun(alpe::playback_underrun_event e)
{
    auto message = QString("playback underrun (%1) at frame %2").arg(QString(e.hard_underrun ? "hard" : "soft"), QString::number(e.position));
    d->win->debugAppend(QtWarningMsg, message);
}

void AlpeInterface::report_error(alpe::error_event e)
{
    QString message;
    switch( e.type ) {
    case alpe::error_event::Type::Alsa:
        message = QString("Alsa error: %1").arg(snd_strerror(e.alsa_errno));
        break;
    case alpe::error_event::Type::Disconnected:
        message = "device disconnected";
        break;
    case alpe::error_event::Type::Protocol:
        message = "remote device did not respond in the alpe stream protocol";
        break;
    case alpe::error_event::Type::UnsupportedLowLatencyHardware:
        message = "audio hardware does not support large enough buffers";
        break;
    case alpe::error_event::Type::Api:
        message = "API error";
        break;
    case alpe::error_event::Type::Internal:
        message = "internal error";
        break;
    }
    d->win->debugAppend(QtCriticalMsg, message);
}

void AlpeInterface::log(alpe::LogLevel level, const char* message)
{
    QtMsgType t = QtFatalMsg;
    switch( level ) {
    case alpe::LogLevel::Info:
        t = QtInfoMsg;
        break;
    case alpe::LogLevel::Warning:
        t = QtWarningMsg;
        break;
    case alpe::LogLevel::Error:
        t = QtCriticalMsg;
        break;
    }
    d->win->debugAppend(t, message);
}
