/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SEEKBAR_H
#define SEEKBAR_H

#include <QWidget>

struct WaveformPreview;

class SeekBar : public QWidget {
    Q_OBJECT
public:
    explicit SeekBar(QWidget* parent = nullptr);

    void setFileData(int64_t frames, unsigned rate);
    int64_t frames() const { return frameCount; }
    unsigned rate() const { return sampleRate; }

    void setFramePosition(int64_t pos);
    int64_t position() const { return framePos; }

    void setWaveformPreview(const WaveformPreview* preview, unsigned endSample);

    QString createTimeLabel();

    QSize sizeHint() const override;

signals:
    void requestSeek(int64_t pos);

protected:
    void leaveEvent(QEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

private:
    QString formatTime(int64_t pos);
    void setMouseXPosition(float pos);

    int64_t framePos = -1;
    int64_t frameCount = -1;
    unsigned sampleRate = 0;
    float mouseXPosition = -1;
    const WaveformPreview* waveformPreview = {};
    unsigned previewEndSample = 0;
};

#endif // SEEKBAR_H
