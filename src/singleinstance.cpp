/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "singleinstance.h"

#include <QDBusConnection>
#include <QDBusError>
#include <QDBusInterface>
#include <QFileInfo>


static const char ServiceName[] = FREEDESKTOP_APP_ID ".SingleInstance";

SingleInstance::SingleInstance(QObject* parent) : QObject{parent} {}

bool SingleInstance::makeSingleInstace(const QStringList& args)
{
    if( args.size() > 2 )
        qFatal("Usage: %s [PATH]", qPrintable(args.first()));
    pendingOpen = args.size() == 2 ? QFileInfo(args.at(1)).absoluteFilePath() : QString();

    auto connection = QDBusConnection::sessionBus();

    if( ! connection.isConnected() ) {
        qWarning("Cannot connect to the D-Bus session bus.");
        return true;
    }

    if( connection.registerService(ServiceName) ) {
        connection.registerObject("/", this, QDBusConnection::ExportAllSlots);
        return true;
    }

    QDBusInterface iface(ServiceName, "/");
    if( ! iface.isValid() ) {
        qWarning("Cannot register nor call D-Bus service.");
        return true;
    }

    iface.call("open", pendingOpen);
    return false;
}

void SingleInstance::unregister()
{
    QDBusConnection::sessionBus().unregisterService(ServiceName);
}

void SingleInstance::open(const QString& arg)
{
    emit requestOpen(arg);
}
