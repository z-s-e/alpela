/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FFMPEGTHREAD_H
#define FFMPEGTHREAD_H

#include <lbu/alsa/alsa.h>
#include <lbu/poll.h>

class FfmpegThread {
public:
    struct BufferRequestResult {
        unsigned framesRead = 0;
        bool eos = false;
        bool error = false;
    };

    FfmpegThread();

    bool open(const char* path);

    lbu::alsa::pcm_format format();
    int64_t frameCount();

    void setDestinationType(snd_pcm_format_t t);
    void seek(int64_t pos);

    pollfd bufferReadyPoll();
    void requestBuffer(lbu::array_ref<void> buf);
    BufferRequestResult requestResult();

    void injectLongDelay();

    ~FfmpegThread();
    FfmpegThread(const FfmpegThread&) = delete;
    FfmpegThread& operator=(const FfmpegThread&) = delete;

private:
    struct Data;
    Data* d = {};
};

#endif // FFMPEGTHREAD_H
