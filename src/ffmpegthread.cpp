/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ffmpegthread.h"

#include "ffmpeg_wrapper.h"

#include <atomic>
#include <cassert>
#include <cfenv>
#include <cmath>
#include <lbu/dynamic_memory.h>
#include <lbu/eventfd.h>
#include <pthread.h>
#include <unistd.h>


namespace {

enum Command {
    NoCommand,
    CommandQuit,
    CommandOpen,
    CommandBuffer
};

struct ffmpeg_type_adapter {
    ffmpeg_wrapper_file_info info = {SND_PCM_FORMAT_UNKNOWN, 0, 0, 0, 0};
    snd_pcm_format_t dst_type = SND_PCM_FORMAT_UNKNOWN;
    unsigned dst_sample_size = 0;
    char* dst = {};
    unsigned count = 0;
    bool done = false;
};

static void copy_read_adapter(const uint8_t* const * sample_data, unsigned frame_offset, unsigned frame_count, void* user_data)
{
    auto adapter = static_cast<ffmpeg_type_adapter*>(user_data);
    if( sample_data == nullptr ) {
        adapter->done = true;
        return;
    }
    const auto channels = adapter->info.channels;
    const auto sample_size = adapter->dst_sample_size;
    const size_t frame_size = size_t(channels) * sample_size;
    if( adapter->info.is_planar && channels > 1 ) {
        size_t src_byte_offset = sample_size * frame_offset;
        for( unsigned i = 0; i < frame_count; ++i ) {
            for( int j = 0; j < channels; ++j ) {
                std::memcpy(adapter->dst, sample_data[j] + src_byte_offset, sample_size);
                adapter->dst += sample_size;
            }
            src_byte_offset += sample_size;
        }
    } else {
        const size_t src_byte_offset = frame_size * frame_offset;
        const size_t bytes = frame_count * frame_size;
        std::memcpy(adapter->dst, sample_data[0] + src_byte_offset, bytes);
        adapter->dst += bytes;
    }
    adapter->count += frame_count;
}

static double read_double_from_i16(const void* data)
{
    return double(lbu::byte_reinterpret_cast<int16_t>(data)) / (uint32_t(1) << 15);
}

static double read_double_from_i32(const void* data)
{
    return double(lbu::byte_reinterpret_cast<int32_t>(data)) / (uint32_t(1) << 31);
}

static double read_double_from_float(const void* data)
{
    return lbu::byte_reinterpret_cast<float>(data);
}

static double read_double_from_double(const void* data)
{
    return lbu::byte_reinterpret_cast<double>(data);
}

template< int Bits >
static int32_t double_to_i32(double val, bool use_safe_factor)
{
    const double factor = std::exp2(Bits - 1);
    return std::clamp<double>(std::rint(val * (factor - (use_safe_factor ? 1 : 0))), -factor, factor - 1);
}

static void convert_read_adapter(const uint8_t* const * sample_data, unsigned frame_offset, unsigned frame_count, void* user_data)
{
    auto adapter = static_cast<ffmpeg_type_adapter*>(user_data);
    if( sample_data == nullptr ) {
        adapter->done = true;
        return;
    }

    const auto channels = adapter->info.channels;
    const bool is_planar = adapter->info.is_planar;
    const auto src_type = adapter->info.type;
    const auto src_sample_size = lbu::alsa::pcm_format::packed_byte_size(src_type);
    const auto dst_type = adapter->dst_type;
    const auto dst_sample_size = adapter->dst_sample_size;
    const size_t samples_in_buffer = frame_count * size_t(is_planar ? 1 : channels);
    const int buffer_count = (is_planar ? channels : 1);

    const bool src_is_float = (src_type == SND_PCM_FORMAT_FLOAT) || (src_type == SND_PCM_FORMAT_FLOAT64);
    const bool use_safe_factor = src_is_float || (dst_sample_size < src_sample_size);

    size_t src_byte_offset = src_sample_size * frame_offset * size_t(is_planar ? 1 : channels);
    lbu::alsa::pcm_buffer::channel_iter dst_iter(adapter->dst, dst_sample_size);
    for( size_t i = 0; i < samples_in_buffer; ++i ) {
        for( int j = 0; j < buffer_count; ++j ) {
            double val = 0;
            const uint8_t* src = sample_data[j] + src_byte_offset;
            switch( src_type ) {
            case SND_PCM_FORMAT_S16:
                val = read_double_from_i16(src);
                break;
            case SND_PCM_FORMAT_S32:
                val = read_double_from_i32(src);
                break;
            case SND_PCM_FORMAT_FLOAT:
                val = read_double_from_float(src);
                break;
            case SND_PCM_FORMAT_FLOAT64:
                val = read_double_from_double(src);
                break;
            default:
                assert(false);
            }

            switch( dst_type ) {
            case SND_PCM_FORMAT_S16_LE:
            case SND_PCM_FORMAT_S16_BE:
                dst_iter.write_i16(double_to_i32<16>(val, use_safe_factor), dst_type);
                break;
            case SND_PCM_FORMAT_S18_3LE:
            case SND_PCM_FORMAT_S18_3BE:
                dst_iter.write_i32(double_to_i32<18>(val, use_safe_factor), dst_type);
                break;
            case SND_PCM_FORMAT_S20_LE:
            case SND_PCM_FORMAT_S20_BE:
            case SND_PCM_FORMAT_S20_3LE:
            case SND_PCM_FORMAT_S20_3BE:
                dst_iter.write_i32(double_to_i32<20>(val, use_safe_factor), dst_type);
                break;
            case SND_PCM_FORMAT_S24_LE:
            case SND_PCM_FORMAT_S24_BE:
            case SND_PCM_FORMAT_S24_3LE:
            case SND_PCM_FORMAT_S24_3BE:
                dst_iter.write_i32(double_to_i32<24>(val, use_safe_factor), dst_type);
                break;
            case SND_PCM_FORMAT_S32_LE:
            case SND_PCM_FORMAT_S32_BE:
                dst_iter.write_i32(double_to_i32<32>(val, use_safe_factor), dst_type);
                break;
            case SND_PCM_FORMAT_FLOAT_LE:
            case SND_PCM_FORMAT_FLOAT_BE:
                dst_iter.write_f(val, dst_type);
                break;
            case SND_PCM_FORMAT_FLOAT64_LE:
            case SND_PCM_FORMAT_FLOAT64_BE:
                dst_iter.write_d(val, dst_type);
                break;
            default:
                assert(false);
            }

            ++dst_iter;
        }
        src_byte_offset += src_sample_size;
    }
    adapter->dst = static_cast<char*>(dst_iter.data());
    adapter->count += frame_count;
}

static void read_event_fd(lbu::fd fd)
{
    if( lbu::event_fd::read_value(fd) != lbu::event_fd::MaximumValue )
        assert(false);
}
static void write_event_fd(lbu::fd fd)
{
    if( ! lbu::event_fd::write_value(fd, lbu::event_fd::MaximumValue) )
        assert(false);
}


struct ffmpeg_thread_shared_data {
    lbu::unique_fd command_fd;
    Command command = NoCommand;

    lbu::alsa::pcm_format format;
    snd_pcm_format_t destination_type = SND_PCM_FORMAT_UNKNOWN;
    int64_t frame_count = -1;

    const char* file_path = {};
    int64_t seek = -1;
    lbu::array_ref<void> buffer;
    FfmpegThread::BufferRequestResult result;

    std::atomic_bool long_delay = false;
};


struct ffmpeg_thread_private_main_loop {
    ffmpeg_thread_shared_data* shared = {};
    lbu::unique_ptr_c<ffmpeg_wrapper, ffmpeg_wrapper_free> ffmpeg;
    ffmpeg_wrapper_file_info file_info = {SND_PCM_FORMAT_UNKNOWN, 0, 0, 0, 0};

    void handle_open()
    {
        file_info = {SND_PCM_FORMAT_UNKNOWN, 0, 0, 0, 0};
        shared->format = {};
        shared->frame_count = -1;
        shared->seek = -1;
        ffmpeg.reset();
        ffmpeg.reset(ffmpeg_wrapper_create(shared->file_path));
        if( ! ffmpeg )
            return;
        auto info = ffmpeg_wrapper_get_file_info(ffmpeg.get());
        if( info.type == SND_PCM_FORMAT_UNKNOWN || info.channels < 1 || info.rate < 8000 )
            return;
        file_info = info;
        shared->format = lbu::alsa::pcm_format{ info.type, uint16_t(info.channels), uint32_t(info.rate) };
        if( info.length > 0 )
            shared->frame_count = info.length;
    }

    void handle_buffer()
    {
        if( shared->long_delay ) {
            sleep(10);
            shared->long_delay = false;
        }

        if( shared->seek >= 0 ) {
            if( ffmpeg_wrapper_seek(ffmpeg.get(), shared->seek) != 0 ) {
                shared->result.error = true;
                return;
            }
            shared->seek = -1;
        }

        ffmpeg_type_adapter adapter{};
        adapter.info = file_info;
        adapter.dst_type = shared->destination_type;
        adapter.dst_sample_size = lbu::alsa::pcm_format::packed_byte_size(adapter.dst_type);
        const size_t frame_byte_size = adapter.dst_sample_size * size_t(adapter.info.channels);
        adapter.dst = shared->buffer.array_static_cast<char>().data();
        assert(shared->buffer.byte_size() % frame_byte_size == 0);
        int frames = shared->buffer.byte_size() / frame_byte_size;

        if( ffmpeg_wrapper_get_frames(ffmpeg.get(), frames,
                                      (adapter.info.type == adapter.dst_type ? copy_read_adapter : convert_read_adapter),
                                      &adapter) != 0 ) {
            shared->result.error = true;
            return;
        }
        shared->result.framesRead = unsigned(adapter.count);
        shared->result.eos = adapter.done;
    }

    void run()
    {
        auto pfd = lbu::poll::poll_fd(shared->command_fd.get(), lbu::poll::FlagsReadReady);
        while( true ) {
            lbu::poll::wait_for_event(pfd);

            switch( shared->command ) {
            case NoCommand:
                assert(false);
            case CommandQuit:
                return;
            case CommandOpen:
                handle_open();
                break;
            case CommandBuffer:
                handle_buffer();
                break;
            }

            read_event_fd(lbu::fd(pfd.fd));
        }
    }
};


static void* ffmpeg_thread_main(void* shared_data_ptr)
{
    pthread_setname_np(pthread_self(), "ffmpeg_thread");
    std::fesetround(FE_TONEAREST);
    ffmpeg_thread_private_main_loop loop;
    loop.shared = static_cast<ffmpeg_thread_shared_data*>(shared_data_ptr);
    loop.run();
    return {};
}

} // namespace


struct FfmpegThread::Data {
    ffmpeg_thread_shared_data shared;
    pthread_t thread = {};

    ~Data()
    {
        shared.command = CommandQuit;
        write_event_fd(shared.command_fd.get());
        if( int err = pthread_join(thread, nullptr); err != 0 )
            lbu::unexpected_system_error(err);
    }

    Data()
    {
        shared.command_fd = lbu::event_fd::create(0, lbu::event_fd::FlagsNonBlock);
        if( int err = pthread_create(&thread, nullptr, ffmpeg_thread_main, &shared); err != 0 )
            lbu::unexpected_system_error(err);
    }
};

FfmpegThread::FfmpegThread() : d(new Data) {}
FfmpegThread::~FfmpegThread() { delete d; }

bool FfmpegThread::open(const char* path)
{
    d->shared.file_path = path;
    d->shared.command = CommandOpen;
    write_event_fd(d->shared.command_fd.get());
    lbu::poll::wait_for_event(bufferReadyPoll());
    d->shared.file_path = nullptr;
    return d->shared.format.type != SND_PCM_FORMAT_UNKNOWN;
}

lbu::alsa::pcm_format FfmpegThread::format() { return d->shared.format; }
int64_t FfmpegThread::frameCount() { return d->shared.frame_count; }
void FfmpegThread::setDestinationType(snd_pcm_format_t t) { d->shared.destination_type = t; }
void FfmpegThread::seek(int64_t pos) { d->shared.seek = pos; }
pollfd FfmpegThread::bufferReadyPoll() { return lbu::poll::poll_fd(d->shared.command_fd.get(), lbu::poll::FlagsWriteReady); }

void FfmpegThread::requestBuffer(lbu::array_ref<void> buf)
{
    d->shared.buffer = buf;
    d->shared.command = CommandBuffer;
    write_event_fd(d->shared.command_fd.get());
}

FfmpegThread::BufferRequestResult FfmpegThread::requestResult()
{
    auto r = d->shared.result;
    assert(r.error || r.eos || r.framesRead > 0);
    d->shared.result = {};
    return r;
}

void FfmpegThread::injectLongDelay()
{
    d->shared.long_delay = true;
}
