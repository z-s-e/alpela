/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "debuglog.h"

#include <QContextMenuEvent>
#include <QMenu>
#include <QTextDocument>

DebugLog::DebugLog(QWidget* parent) : QTextEdit{parent}
{
    document()->setMaximumBlockCount(1000);
}

void DebugLog::contextMenuEvent(QContextMenuEvent* event)
{
    QMenu* menu = createStandardContextMenu();
    menu->addSeparator();
    menu->addAction(tr("Inject long IO delay"), this, &DebugLog::requestIODelay);
    menu->addAction(tr("Clear"), this, &QTextEdit::clear);
    menu->exec(event->globalPos());
    delete menu;
}
