/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "mainwindow.h"
#include "singleinstance.h"

#include <QApplication>
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    SingleInstance singleInstance;
    if( ! singleInstance.makeSingleInstace(app.arguments()) )
        return 0;

    app.setApplicationName(PROJECT_NAME);
    app.setApplicationVersion(PROJECT_VERSION);
    app.setOrganizationName("z-s-e");
    app.setWindowIcon(QIcon("://icon.svg"));
    app.setDesktopFileName(FREEDESKTOP_APP_ID);

    MainWindow w;
    w.setWindowTitle(QString("%1 v%2").arg(app.applicationName(), app.applicationVersion()));
    singleInstance.connect(&singleInstance, &SingleInstance::requestOpen, &w, &MainWindow::openFile);
    singleInstance.connect(&app, &QCoreApplication::aboutToQuit, &singleInstance, &SingleInstance::unregister);

    w.show();
    w.openFile(singleInstance.pendingOpen);

    return app.exec();
}
