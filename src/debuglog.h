/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DEBUGLOG_H
#define DEBUGLOG_H

#include <QTextEdit>

class DebugLog : public QTextEdit {
    Q_OBJECT
public:
    DebugLog(QWidget* parent = nullptr);

signals:
    void requestIODelay();

protected:
    void contextMenuEvent(QContextMenuEvent* event) override;
};

#endif // DEBUGLOG_H
