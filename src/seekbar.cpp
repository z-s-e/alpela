/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "seekbar.h"

#include "waveformpreview.h"

#include <lbu/math.h>
#include <QFontMetrics>
#include <QMouseEvent>
#include <QPainter>
#include <stdio.h>


static int pixelFromPosition(int width, int64_t pos, int64_t count)
{
    return qBound(0, int((double(pos) * width) / count), width);
}

static int64_t positionFromPixel(int width, float x, int64_t count)
{
    return qBound(int64_t(0), int64_t(double(x) * count / width), count);
}


SeekBar::SeekBar(QWidget* parent) : QWidget{parent}
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    setMouseTracking(true);
    setAttribute(Qt::WA_OpaquePaintEvent);
}

void SeekBar::setFileData(int64_t frames, unsigned int rate)
{
    framePos = 0;
    frameCount = frames;
    sampleRate = rate;
    update();
}

void SeekBar::setFramePosition(int64_t pos)
{
    framePos = pos;
    update();
}

void SeekBar::setWaveformPreview(const WaveformPreview* preview, unsigned endSample)
{
    if( waveformPreview != preview ) {
        waveformPreview = preview;
        updateGeometry();
    }
    previewEndSample = endSample;
    update();
}

QString SeekBar::createTimeLabel() { return QString("%1 / %2").arg(formatTime(framePos), formatTime(frameCount)); }

QSize SeekBar::sizeHint() const
{
    const auto fontHeight = fontMetrics().height();
    return {1, waveformPreview == nullptr ? (2 * fontHeight) : (1 + 4 * fontHeight) };
}

void SeekBar::leaveEvent(QEvent*)
{
    setMouseXPosition(-1);
}

void SeekBar::mousePressEvent(QMouseEvent*) {}

void SeekBar::mouseReleaseEvent(QMouseEvent* event)
{
    if( frameCount <= 0 )
        return;
    if( event->button() != Qt::LeftButton || event->buttons() != Qt::NoButton )
        return;
    emit requestSeek(positionFromPixel(width(), mouseXPosition, frameCount));
}

void SeekBar::mouseMoveEvent(QMouseEvent* event)
{
    setMouseXPosition(qMax(event->position().x(), 0.0f));
}

void SeekBar::paintEvent(QPaintEvent*)
{
    const auto p = palette();
    const auto r = rect();

    QPainter painter(this);
    painter.fillRect(r, p.color(QPalette::Button));

    if( frameCount <= 0 )
        return;

    painter.fillRect(QRect(0, 0, pixelFromPosition(r.width(), framePos, frameCount), r.height()), p.color(QPalette::Highlight));

    if( waveformPreview ) {
        QColor col = p.color(QPalette::Text);
        col.setAlphaF(0.5);
        painter.setPen(QPen(col));
        const int fontHeight = fontMetrics().height();
        const int mid = 2 * fontHeight;
        const unsigned w = unsigned(r.width());
        for( unsigned x = 0; x < w; ++x ) {
            const unsigned start = (x * WaveformPreviewSamples) / w;
            if( start >= previewEndSample )
                break;
            const unsigned end = qMin(lbu::idiv_ceil((x + 1) * WaveformPreviewSamples, w), previewEndSample);
            WaveformPreview::Range r = waveformPreview->data[start];
            for( unsigned i = start + 1; i < end; ++i ) {
                r.max = qMax(r.max, waveformPreview->data[i].max);
                r.min = qMin(r.min, waveformPreview->data[i].min);
            }
            painter.drawLine(int(x), mid + lbu::idiv_ceil(mid * r.max, 128),
                             int(x), mid + lbu::idiv_floor(mid * r.min, 128));
        }
    }

    if( mouseXPosition >= 0 ) {
        painter.fillRect(QRect(qMin(int(mouseXPosition), r.width() - 1), 0, 1, r.height()), p.color(QPalette::Text));
        const auto fm = fontMetrics();
        const auto label = formatTime(positionFromPixel(r.width(), mouseXPosition, frameCount));
        const auto seekLabelSize = fm.size(Qt::TextSingleLine, label).grownBy(QMargins(1, 0, 1, 0));
        const auto seekLabelPos = QPoint(qMax(0, int(mouseXPosition) - seekLabelSize.width()), 0);
        painter.fillRect(QRect(seekLabelPos, seekLabelSize), p.color(QPalette::Text));
        painter.setPen(QPen(p.color(QPalette::Base)));
        painter.drawText(seekLabelPos.x() + 1, seekLabelSize.height() - fm.descent(), label);
    }
}

QString SeekBar::formatTime(int64_t pos)
{
    if( sampleRate == 0 )
        return "-";

    if( pos < 0 )
        return "?";

    const auto secs = pos / sampleRate;
    char buf[100] = {};

    if( secs / (60 * 60) == 0 )
        snprintf(buf, sizeof(buf), "%02d:%02d", int((secs / 60) % 60), int(secs % 60));
    else
        snprintf(buf, sizeof(buf), "%ld:%02d:%02d", long(secs / (60 * 60)), int((secs / 60) % 60), int(secs % 60));
    return buf;
}

void SeekBar::setMouseXPosition(float pos)
{
    mouseXPosition = pos;
    update();
}
