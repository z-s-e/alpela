#include "waveformpreviewgenerator.h"

#include "ffmpeg_wrapper.h"

#include <algorithm>
#include <cmath>
#include <lbu/alsa/alsa.h>
#include <lbu/eventfd.h>
#include <lbu/poll.h>
#include <pthread.h>
#include <QMetaMethod>


namespace {

constexpr unsigned FrameAbortCheckCount = 1000000;


template< typename T >
static int8_t f_to_i8(T val)
{
    const T absmax = 127;
    return std::clamp<int8_t>(std::rint(val * absmax), -absmax, absmax);
}

static int8_t read_from_i16(const void* data)
{
    return std::max<int8_t>(-127, lbu::byte_reinterpret_cast<int16_t>(data) / (int32_t(1) << 8));
}

static int8_t read_from_i32(const void* data)
{
    return std::max<int8_t>(-127, lbu::byte_reinterpret_cast<int32_t>(data) / (int32_t(1) << 24));
}

static int8_t read_from_float(const void* data)
{
    return f_to_i8(lbu::byte_reinterpret_cast<float>(data));
}

static int8_t read_from_double(const void* data)
{
    return f_to_i8(lbu::byte_reinterpret_cast<double>(data));
}

static void add_value_to_range(WaveformPreview::Range& r, int8_t val)
{
    r = {std::min(r.min, val), std::max(r.max, val)};
}

static int64_t frames_for_preview_sample(int64_t total_frames, unsigned sample_index)
{
    const int tmp = WaveformPreviewSamples / 2;
    const int start = ((sample_index * total_frames) + tmp) / WaveformPreviewSamples;
    const int end = (((sample_index + 1) * total_frames) + tmp) / WaveformPreviewSamples;
    return end - start;
}

static void read_event_fd(lbu::fd fd)
{
    if( lbu::event_fd::read_value(fd) != lbu::event_fd::MaximumValue )
        assert(false);
}
static void write_event_fd(lbu::fd fd)
{
    if( ! lbu::event_fd::write_value(fd, lbu::event_fd::MaximumValue) )
        assert(false);
}


struct ffmpeg_callback_data {
    ffmpeg_wrapper_file_info info = {SND_PCM_FORMAT_UNKNOWN, 0, 0, 0, 0};
    WaveformPreview* preview = {};
    unsigned current_index = 0;
};


static void add_frames_current_range(const uint8_t* const * sample_data, unsigned frame_offset, unsigned frame_count, void* user_data)
{
    auto data = static_cast<ffmpeg_callback_data*>(user_data);
    WaveformPreview::Range& r = data->preview->data[data->current_index];

    if( sample_data == nullptr ) {
        add_value_to_range(r, 0);
        return;
    }

    const auto channels = data->info.channels;
    const bool is_planar = data->info.is_planar;
    const auto src_type = data->info.type;
    const auto src_sample_size = lbu::alsa::pcm_format::packed_byte_size(src_type);

    size_t src_byte_offset = src_sample_size * frame_offset * size_t(is_planar ? 1 : channels);

    const size_t samples_in_buffer = frame_count * size_t(is_planar ? 1 : channels);
    const int buffer_count = (is_planar ? channels : 1);

    for( size_t i = 0; i < samples_in_buffer; ++i ) {
        for( int j = 0; j < buffer_count; ++j ) {
            const uint8_t* src = sample_data[j] + src_byte_offset;
            int8_t val;
            switch( src_type ) {
            case SND_PCM_FORMAT_S16:
                val = read_from_i16(src);
                break;
            case SND_PCM_FORMAT_S32:
                val = read_from_i32(src);
                break;
            case SND_PCM_FORMAT_FLOAT:
                val = read_from_float(src);
                break;
            case SND_PCM_FORMAT_FLOAT64:
                val = read_from_double(src);
                break;
            default:
                assert(false);
            }
            add_value_to_range(r, val);
        }
        src_byte_offset += src_sample_size;
    }
}

static void first_frame_set_range(const uint8_t* const * sample_data, unsigned frame_offset, unsigned frame_count, void* user_data)
{
    auto data = static_cast<ffmpeg_callback_data*>(user_data);
    WaveformPreview::Range& r = data->preview->data[data->current_index];

    if( sample_data == nullptr ) {
        r = {0, 0};
        return;
    }
    assert(frame_count == 1);

    const auto channels = data->info.channels;
    const bool is_planar = data->info.is_planar;
    const auto src_type = data->info.type;
    const auto src_sample_size = lbu::alsa::pcm_format::packed_byte_size(src_type);

    size_t src_byte_offset = src_sample_size * frame_offset * size_t(is_planar ? 1 : channels);
    const uint8_t* src = sample_data[0] + src_byte_offset;
    int8_t val;

    switch( src_type ) {
    case SND_PCM_FORMAT_S16:
        val = read_from_i16(src);
        break;
    case SND_PCM_FORMAT_S32:
        val = read_from_i32(src);
        break;
    case SND_PCM_FORMAT_FLOAT:
        val = read_from_float(src);
        break;
    case SND_PCM_FORMAT_FLOAT64:
        val = read_from_double(src);
        break;
    default:
        assert(false);
    }

    r = {val, val};

    add_frames_current_range(sample_data, frame_offset, frame_count, user_data);
}


struct WaveformPreviewSharedThreadData {
    lbu::unique_fd command_fd;
    bool quit = false;
    std::atomic_bool cancel;

    QString file;
    WaveformPreview* preview = {};
    WaveformPreviewGenerator* generator = {};
};


static bool report_result(WaveformPreviewSharedThreadData* shared, WaveformPreviewGenerator::Result result)
{
    const auto finishedSignal = QMetaMethod::fromSignal(&WaveformPreviewGenerator::finished);
    finishedSignal.invoke(shared->generator, Qt::QueuedConnection, shared->file, result);
    return result == WaveformPreviewGenerator::ResultOk;
}

static bool generate(WaveformPreviewSharedThreadData* shared)
{
    lbu::unique_ptr_c<ffmpeg_wrapper, ffmpeg_wrapper_free> ffmpeg;
    ffmpeg.reset(ffmpeg_wrapper_create(shared->file.toLocal8Bit().data()));
    if( ! ffmpeg )
        return report_result(shared, WaveformPreviewGenerator::ResultError);

    auto info = ffmpeg_wrapper_get_file_info(ffmpeg.get());
    if( info.type == SND_PCM_FORMAT_UNKNOWN || info.channels < 1 || info.rate < 8000 ||
        info.length < WaveformPreviewSamples || info.length >= std::numeric_limits<int64_t>::max() / WaveformPreviewSamples ) {
        return report_result(shared, WaveformPreviewGenerator::ResultError);
    }

    ffmpeg_callback_data cb;
    cb.info = info;
    cb.preview = shared->preview;

    const auto progressSignal = QMetaMethod::fromSignal(&WaveformPreviewGenerator::progress);

    for( ; cb.current_index < WaveformPreviewSamples; ++cb.current_index ) {
        auto frames = frames_for_preview_sample(info.length, cb.current_index);
        if( ffmpeg_wrapper_get_frames(ffmpeg.get(), 1, first_frame_set_range, &cb) != 0 ) {
            return report_result(shared, WaveformPreviewGenerator::ResultError);
        }
        --frames;
        while( frames > 0 ) {
            auto tmp = std::min<int64_t>(frames, FrameAbortCheckCount);
            if( ffmpeg_wrapper_get_frames(ffmpeg.get(), tmp, add_frames_current_range, &cb) != 0 ) {
                return report_result(shared, WaveformPreviewGenerator::ResultError);
            }
            frames -= tmp;
            if( shared->cancel )
                return report_result(shared, WaveformPreviewGenerator::ResultCanceled);
        }
        progressSignal.invoke(shared->generator, Qt::QueuedConnection, shared->file, cb.current_index + 1);
    }
    return report_result(shared, WaveformPreviewGenerator::ResultOk);
}


static void* WaveformPreview_thread_main(void* shared_data_ptr)
{
    pthread_setname_np(pthread_self(), "WaveformPreview_thread");
    auto shared = static_cast<WaveformPreviewSharedThreadData*>(shared_data_ptr);
    auto pfd = lbu::poll::poll_fd(shared->command_fd.get(), lbu::poll::FlagsReadReady);
    while( true ) {
        lbu::poll::wait_for_event(pfd);
        if( shared->quit )
            return {};
        generate(shared);
        read_event_fd(lbu::fd(pfd.fd));
    }
}

} // namespace


struct WaveformPreviewGenerator::Data {
    WaveformPreviewSharedThreadData shared;
    pthread_t thread = {};

    void waitReady()
    {
        lbu::poll::wait_for_event(lbu::poll::poll_fd(shared.command_fd.get(), lbu::poll::FlagsWriteReady));
    }

    ~Data()
    {
        shared.cancel = true;
        waitReady();
        shared.quit = true;
        write_event_fd(shared.command_fd.get());
        if( int err = pthread_join(thread, nullptr); err != 0 )
            lbu::unexpected_system_error(err);
    }

    Data(WaveformPreviewGenerator* gen)
    {
        shared.generator = gen;
        shared.preview = &gen->preview;
        shared.command_fd = lbu::event_fd::create(0, lbu::event_fd::FlagsNonBlock);
        if( int err = pthread_create(&thread, nullptr, WaveformPreview_thread_main, &shared); err != 0 )
            lbu::unexpected_system_error(err);
    }
};

WaveformPreviewGenerator::WaveformPreviewGenerator(QObject *parent) : QObject{parent}, d(new Data(this)) {}
WaveformPreviewGenerator::~WaveformPreviewGenerator() { delete d; }

void WaveformPreviewGenerator::requestPreview(const QString& file)
{
    d->waitReady();
    d->shared.file = file;
    d->shared.cancel = false;
    write_event_fd(d->shared.command_fd.get());
}

void WaveformPreviewGenerator::cancelPreview()
{
    d->shared.cancel = true;
}
