/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <alpe_thread.h>
#include <memory>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
class QLabel;
class QSettings;
class QTimer;
QT_END_NAMESPACE

class AlpeInterface;
class WaveformPreviewGenerator;


class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    struct DefaultFade {
        alpe::fade_parameter::Type type = alpe::fade_parameter::Type::Moderate;
        std::chrono::milliseconds duration = std::chrono::milliseconds(30);
    };

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

    void openFile(const QString& filePath);

    void debugAppend(QtMsgType level, const QString& msg); // thread-safe
    void updateFileInfo(int64_t frameCount, unsigned period, unsigned rate); // thread-safe

protected:
    void keyPressEvent(QKeyEvent* event) override;
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dragLeaveEvent(QDragLeaveEvent *) override;
    void dropEvent(QDropEvent* event) override;

private:
    alpe::fade_parameter defaultFadeParam() const;
    void loadFadesTmp(QSettings& s);
    void loadSettings();
    void openSettings();
    void saveSettings();
    void togglePlayPause();
    void stop();
    void rewindStart();
    void fade(const QString& fadeString);
    void seek(int64_t pos);
    void openFileIfPending();
    void updateState();
    void togglePreview();

    std::unique_ptr<Ui::MainWindow> ui;
    QMenu* fadesMenu = {};
    QLabel* labelState = {};
    QLabel* labelTime = {};
    QTimer* updateTimer = {};
    std::unique_ptr<AlpeInterface> interface;
    std::unique_ptr<alpe_thread> alpeThread;
    WaveformPreviewGenerator* previewGenerator = {};
    alpe_thread::State lastState = alpe_thread::State::Initial;
    unsigned interpolateLimit = 0;

    DefaultFade defaultFade = {};
    QStringList settingsFadesTmp;

    QString openPending;
    bool previewPending = false;
};
#endif // MAINWINDOW_H
