#ifndef WAVEFORMPREVIEW_H
#define WAVEFORMPREVIEW_H

#include <stdint.h>

constexpr unsigned WaveformPreviewSamples = 4096;

struct WaveformPreview {
    struct Range {
        int8_t min = 0;
        int8_t max = 0;
    };

    Range data[WaveformPreviewSamples] = {};
};

#endif // WAVEFORMPREVIEW_H
