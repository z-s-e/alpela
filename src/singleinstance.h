/* Copyright 2024-2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SINGLEINSTANCE_H
#define SINGLEINSTANCE_H

#include <QObject>

class SingleInstance : public QObject {
    Q_OBJECT
public:
    explicit SingleInstance(QObject* parent = nullptr);

    bool makeSingleInstace(const QStringList& args);
    void unregister();

    QString pendingOpen;

public slots:
    void open(const QString& arg);

signals:
    void requestOpen(const QString& arg);
};

#endif // SINGLEINSTANCE_H
