/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ffmpeg_wrapper.h"

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>


static void print_unexpected(const char* fmt, ...) __attribute__ (( format( __printf__, 1, 2 )));
static void print_unexpected(const char* fmt, ...)
{
    fprintf(stderr, "ffmpeg wrapper unexpected error: ");
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
}

struct ffmpeg_wrapper {
    AVFormatContext* fmt_ctx;
    AVCodecContext* dec_ctx;
    int stream_idx;
    AVStream* stream;
    AVPacket* packet;
    AVFrame* frame;
    ffmpeg_wrapper_file_info info;
    int frame_frame_offset;
    int need_time_conv;
};

struct ffmpeg_wrapper* ffmpeg_wrapper_create(const char* path)
{
    struct ffmpeg_wrapper* w = calloc(1, sizeof(struct ffmpeg_wrapper));
    if( w == NULL )
        return NULL;

    const AVCodec* dec = NULL;
    AVCodecContext* dec_ctx = NULL;
    ffmpeg_wrapper_file_info info;

    int ret = avformat_open_input(&w->fmt_ctx, path, NULL, NULL);
    if( ret < 0 ) {
        print_unexpected("open input %s failed - %s", path, av_err2str(ret));
        goto error;
    }

    ret = avformat_find_stream_info(w->fmt_ctx, NULL);
    if( ret < 0 ) {
        print_unexpected("find stream info for input %s failed - %s", path, av_err2str(ret));
        goto error;
    }

    ret = av_find_best_stream(w->fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, &dec, 0);
    if( ret < 0 || dec == NULL ) {
        if( ret < 0 )
            print_unexpected("find audio stream for input %s failed - %s", path, av_err2str(ret));
        else
            print_unexpected("create decoder for input %s failed", path);
        goto error;
    }
    w->stream_idx = ret;
    w->stream = w->fmt_ctx->streams[ret];

    w->dec_ctx = avcodec_alloc_context3(dec);
    if( w->dec_ctx == NULL ) {
        print_unexpected("create decoder context for input %s failed", path);
        goto error;
    }

    ret = avcodec_parameters_to_context(w->dec_ctx, w->stream->codecpar);
    if( ret < 0 ) {
        print_unexpected("copy %s audio codec parameters to decoder context failed", path);
        goto error;
    }
    dec_ctx = w->dec_ctx;

    ret = avcodec_open2(dec_ctx, dec, NULL);
    if( ret < 0 ) {
        print_unexpected("open decoder for input %s failed - %s", path, av_err2str(ret));
        goto error;
    }

    switch( dec_ctx->sample_fmt ) {
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        info.type = SND_PCM_FORMAT_S16;
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        info.type = SND_PCM_FORMAT_S32;
        break;
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
        info.type = SND_PCM_FORMAT_FLOAT;
        break;
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        info.type = SND_PCM_FORMAT_FLOAT64;
        break;
    default:
        print_unexpected("input %s audio has unsupported sample format", path);
        goto error;
    }
    info.rate = dec_ctx->sample_rate;
    assert(info.rate > 0);
    info.channels = dec_ctx->ch_layout.nb_channels;
    info.is_planar = av_sample_fmt_is_planar(dec_ctx->sample_fmt);

    w->need_time_conv = (w->stream->time_base.num != 1 || w->stream->time_base.den != info.rate);
    info.length = w->stream->duration;
    if( info.length <= 0 )
        info.length = w->fmt_ctx->duration * info.rate / AV_TIME_BASE;
    else if( w->need_time_conv )
        info.length = (info.length * info.rate * w->stream->time_base.num) / w->stream->time_base.den;

    w->info = info;
    w->frame_frame_offset = -1;

    w->packet = av_packet_alloc();
    if( w->packet == NULL ) {
        print_unexpected("alloc packet failed");
        goto error;
    }

    w->frame = av_frame_alloc();
    if( w->frame == NULL ) {
        print_unexpected("alloc frame failed");
        goto error;
    }

    return w;

error:
    ffmpeg_wrapper_free(w);
    return NULL;
}

void ffmpeg_wrapper_free(struct ffmpeg_wrapper* wrapper)
{
    if( wrapper->frame_frame_offset > -1 )
        av_frame_unref( wrapper->frame);
    if( wrapper->frame )
        av_frame_free(&wrapper->frame);
    if( wrapper->packet )
        av_packet_free(&wrapper->packet);
    if( wrapper->dec_ctx )
        avcodec_free_context(&wrapper->dec_ctx);
    if( wrapper->fmt_ctx )
        avformat_close_input(&wrapper->fmt_ctx);
    free(wrapper);
}

ffmpeg_wrapper_file_info ffmpeg_wrapper_get_file_info(struct ffmpeg_wrapper* wrapper)
{
    return wrapper->info;
}

static int submit_packet(struct ffmpeg_wrapper* w, const AVPacket* pkt)
{
    int ret = avcodec_send_packet(w->dec_ctx, pkt);
    if( ret < 0 ) {
        print_unexpected("submitting a packet for decoding failed: %s", av_err2str(ret));
        return -1;
    }
    return 0;
}

static int push_next_packet(struct ffmpeg_wrapper* w)
{
    while( 1 ) {
        int ret = av_read_frame(w->fmt_ctx, w->packet);
        if( ret < 0 ) {
            if( ret == AVERROR_EOF )
                return submit_packet(w, NULL);
            print_unexpected("read frame error: %s", av_err2str(ret));
            return -1;
        }
        if( w->packet->stream_index == w->stream_idx ) {
            ret = submit_packet(w, w->packet);
            av_packet_unref(w->packet);
            return ret;
        }
        av_packet_unref(w->packet);
    }
}

int ffmpeg_wrapper_get_frames(struct ffmpeg_wrapper* wrapper, int frame_count, push_frames_func psf, void* user_data)
{
    while( 1 ) {
        if( frame_count <= 0 )
            return 0;
        const unsigned count = (unsigned)frame_count;

        const int frame_offset = wrapper->frame_frame_offset;
        if( frame_offset >= 0 ) {
            AVFrame* f = wrapper->frame;

            const int frames_in_frame = f->nb_samples;
            assert(frames_in_frame > frame_offset);
            const unsigned frames_left = (unsigned)(frames_in_frame - frame_offset);
            if( count >= frames_left ) {
                psf((const uint8_t * const*)f->extended_data, (unsigned)frame_offset, frames_left, user_data);
                av_frame_unref(f);
                wrapper->frame_frame_offset = -1;
                frame_count -= (int)frames_left;
                continue;
            } else {
                psf((const uint8_t * const*)f->extended_data, (unsigned)frame_offset, count, user_data);
                wrapper->frame_frame_offset += frame_count;
                return 0;
            }
        }

        int ret;

    receive_frame:
        ret = avcodec_receive_frame(wrapper->dec_ctx, wrapper->frame);
        if( ret < 0 ) {
            if( ret == AVERROR(EAGAIN) ) {
                ret = push_next_packet(wrapper);
                if( ret != 0 )
                    return ret;
                goto receive_frame;
            } else if( ret == AVERROR_EOF ) {
                psf(NULL, 0, 0, user_data);
                return 0;
            }
            print_unexpected("decoding error: %s", av_err2str(ret));
            return -1;
        }
        wrapper->frame_frame_offset = 0;
    }
}

static void skip_frames_callback(const uint8_t* const * sample_data, unsigned frame_offset, unsigned frame_count, void* user_data) {}

int ffmpeg_wrapper_seek(struct ffmpeg_wrapper* wrapper, int64_t pos)
{
    if( wrapper->frame_frame_offset >= 0 ) {
        av_frame_unref(wrapper->frame);
        wrapper->frame_frame_offset = -1;
    }
    int64_t seek_val = pos;
    if( wrapper->need_time_conv == 1 )
        seek_val = (pos * wrapper->stream->time_base.den) / ((int64_t)wrapper->info.rate * wrapper->stream->time_base.num);
    int ret = av_seek_frame(wrapper->fmt_ctx, wrapper->stream_idx, seek_val, AVSEEK_FLAG_BACKWARD);
    if( ret < 0 ) {
        print_unexpected("seek error: %s", av_err2str(ret));
        return -1;
    }
    avcodec_flush_buffers(wrapper->dec_ctx);
    while( 1 ) {
        ret = av_read_frame(wrapper->fmt_ctx, wrapper->packet);
        if( ret < 0 ) {
            print_unexpected("read frame error: %s", av_err2str(ret));
            return -1;
        }
        if( wrapper->packet->stream_index == wrapper->stream_idx ) {
            ret = submit_packet(wrapper, wrapper->packet);
            if( ret == 0 ) {
                int64_t current = wrapper->packet->pts;
                if( wrapper->need_time_conv == 1 )
                    current = (current * wrapper->info.rate * wrapper->stream->time_base.num) / wrapper->stream->time_base.den;
                av_packet_unref(wrapper->packet);
                if( current < pos )
                    ret = ffmpeg_wrapper_get_frames(wrapper, pos - current, skip_frames_callback, NULL);
                else if( current > pos )
                    fprintf(stderr, "ffmpeg wrapper warning: unexpected seek position %ld (requested %ld)\n", (long)current, (long)pos);
            } else {
                av_packet_unref(wrapper->packet);
            }
            return ret;
        }
        av_packet_unref(wrapper->packet);
    }
}
