#include "tcp_stream_with_timeout.h"

#include <arpa/inet.h>
#include <lbu/abstract_stream.h>
#include <lbu/poll.h>
#include <netinet/tcp.h>


namespace {

constexpr auto Timeout = std::chrono::seconds(5);

constexpr size_t BufSize = 128;


static int wait_for_socket(pollfd pfd)
{
    while( true ) {
        auto p = lbu::poll::poll(&pfd, 1, Timeout);
        if( p.status == lbu::poll::StatusPollInterrupted )
            continue;
        if( p.count == 0 )
            return ETIMEDOUT;
        return p.status;
    }
}

static int set_flag_return_error(uint8_t* status_flags, uint8_t flag)
{
    *status_flags |= flag;
    return -1;
}

class tcp_input_stream : public lbu::stream::abstract_input_stream {
public:
    tcp_input_stream() : lbu::stream::abstract_input_stream(lbu::stream::InternalBuffer::Yes)
    {
        buffer_base_ptr = buf;
    }

    void set_descriptor(lbu::fd f)
    {
        filedes = f;
        buffer_available = 0;
        buffer_offset = 0;
        status_flags = 0;
    }

protected:
    ssize_t read_stream(lbu::array_ref<lbu::io::io_vector> buf_array, size_t required_read) override
    {
        if( has_error() )
            return -1;
        if( at_end() )
            return 0;

        const auto mode = (required_read > 0) ? lbu::stream::Mode::Blocking : lbu::stream::Mode::NonBlocking;

        lbu::io::io_vector internal_array[2];
        uint32_t buffer_read = 0;

        assert(buf_array.size() == 1);
        buffer_read = buffer_available;
        assert(buffer_read == 0 || buf_array[0].iov_len > buffer_read);
        if( buffer_read > 0 ) {
            std::memcpy(buf_array[0].iov_base, buffer_base_ptr + buffer_offset, buffer_read);
            buf_array = lbu::io::io_vector_array_advance(buf_array, buffer_read);
            buffer_available = 0;
            if( required_read > 0 ) {
                assert(required_read > buffer_read);
                required_read -= buffer_read;
            }
        }

        if( buf_array[0].iov_len <= BufSize ) {
            internal_array[0] = buf_array[0];
            internal_array[1] = lbu::io::io_vec(buffer_base_ptr, BufSize);
            buf_array = lbu::array_ref<lbu::io::io_vector>(internal_array);
        }

        const size_t first_read_request = buf_array[0].iov_len;

        size_t count = 0;
        while( true ) {
            const auto r = lbu::io::readv(filedes, buf_array);
            if( r.size > 0 ) {
                count += size_t(r.size);

                if( count < required_read ) {
                    buf_array = lbu::io::io_vector_array_advance(buf_array, size_t(r.size));
                    continue;
                }

                if( count > first_read_request ) {
                    buffer_offset = 0;
                    buffer_available = uint32_t(count - first_read_request);
                    return ssize_t(first_read_request + buffer_read);
                }

                return ssize_t(count + buffer_read);
            } else if( r.size == 0 ) {
                status_flags = StatusEndOfStream;
                return ssize_t(count + buffer_read);
            } else if( r.status == lbu::io::ReadWouldBlock ) {
                if( mode == lbu::stream::Mode::NonBlocking )
                    return ssize_t(buffer_read);
                if( wait_for_socket(lbu::poll::poll_fd(filedes, lbu::poll::FlagsReadReady)) != lbu::poll::StatusNoError )
                    return set_flag_return_error(&status_flags, StatusError);
            } else {
                return set_flag_return_error(&status_flags, StatusError);
            }
        }
    }

    lbu::array_ref<const void> get_read_buffer(lbu::stream::Mode mode) override
    {
        if( status_flags )
            return {};

    retry:
        const auto r = lbu::io::read(filedes, lbu::array_ref<char>(buffer_base_ptr, BufSize));
        if( r.size > 0 ) {
            buffer_offset = 0;
            buffer_available = uint32_t(r.size);
            return current_buffer();
        } else if( r.size == 0 ) {
            status_flags = StatusEndOfStream;
        } else if( r.status == lbu::io::ReadWouldBlock ) {
            if( mode == lbu::stream::Mode::NonBlocking )
                return {};
            if( wait_for_socket(lbu::poll::poll_fd(filedes, lbu::poll::FlagsReadReady)) == lbu::poll::StatusNoError )
                goto retry;
            status_flags = StatusError;
        } else {
            status_flags = StatusError;
        }
        return {};
    }

private:
    lbu::fd filedes;
    char buf[BufSize];
};


class tcp_output_stream : public lbu::stream::abstract_output_stream {
public:
    tcp_output_stream() : lbu::stream::abstract_output_stream(lbu::stream::InternalBuffer::Yes)
    {
        buffer_base_ptr = buf;
    }

    void set_descriptor(lbu::fd f)
    {
        filedes = f;
        reset_buffer();
        status_flags = 0;
    }

protected:
    ssize_t write_stream(lbu::array_ref<lbu::io::io_vector> buf_array, lbu::stream::Mode mode) override
    {
        return write_fd(buf_array, mode);
    }

    lbu::array_ref<void> get_write_buffer(lbu::stream::Mode mode) override
    {
        buffer_flush(mode);
        return current_buffer();
    }

    bool write_buffer_flush(lbu::stream::Mode mode) override
    {
        return buffer_flush(mode);
    }

private:
    ssize_t write_fd(lbu::array_ref<lbu::io::io_vector> buf_array, lbu::stream::Mode mode)
    {
        if( status_flags )
            return -1;

        lbu::io::io_vector internal_array[2];
        ssize_t internal_write_size = 0;

        assert(buf_array.size() == 1);
        internal_write_size = ssize_t(buffer_offset - buffer_write_offset);
        if( internal_write_size > 0 ) {
            internal_array[0] = lbu::io::io_vec(buffer_base_ptr + buffer_write_offset, size_t(internal_write_size));
            internal_array[1] = buf_array[0];
            buf_array = lbu::array_ref<lbu::io::io_vector>(internal_array);
        }

        if( mode == lbu::stream::Mode::Blocking ) {
            const ssize_t sum = ssize_t(lbu::io::io_vector_array_size_sum(buf_array));
            ssize_t count = 0;

            while( count < sum ) {
                const auto r = lbu::io::writev(filedes, buf_array);
                if( r.status == lbu::io::WriteWouldBlock ) {
                    if( wait_for_socket(lbu::poll::poll_fd(filedes, lbu::poll::FlagsWriteReady)) != lbu::poll::StatusNoError )
                        return set_flag_return_error(&status_flags, StatusError);
                    continue;
                } else if( r.size < 0 ) {
                    buffer_available = 0;
                    return set_flag_return_error(&status_flags, StatusError);
                }
                count += r.size;
                buf_array = lbu::io::io_vector_array_advance(buf_array, size_t(r.size));
            }

            reset_buffer();

            return sum - internal_write_size;
        } else {
            const auto r = lbu::io::writev(filedes, buf_array);
            if( r.size >= 0 ) {
                if( r.size >= internal_write_size ) {
                    reset_buffer();
                    return r.size - internal_write_size;
                } else {
                    buffer_write_offset += uint32_t(r.size);
                    return 0;
                }
            } else if( r.status == lbu::io::WriteWouldBlock ) {
                return 0;
            } else {
                buffer_available = 0;
                return set_flag_return_error(&status_flags, StatusError);
            }
        }
    }

    bool buffer_flush(lbu::stream::Mode mode)
    {
        auto b = lbu::io::io_vec(nullptr, 0);
        if( write_fd(lbu::array_ref_one_element(&b), mode) < 0 )
            return false;
        return (buffer_offset == 0);
    }

    void reset_buffer()
    {
        buffer_offset = 0;
        buffer_write_offset = 0;
        buffer_available = filedes ? BufSize : 0;
    }

    lbu::fd filedes;
    uint32_t buffer_write_offset;
    char buf[BufSize];
};

} // namespace


struct tcp_stream_with_timeout::data {
    lbu::unique_fd socket;
    tcp_input_stream in;
    tcp_output_stream out;
};


tcp_stream_with_timeout::tcp_stream_with_timeout() {}
tcp_stream_with_timeout::~tcp_stream_with_timeout() { delete d; }

void tcp_stream_with_timeout::reset()
{
    if( d == nullptr )
        d = new data;

    lbu::fd fd(::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0));
    d->socket.reset(fd);
    assert(d->socket);

    d->in.set_descriptor(fd);
    d->out.set_descriptor(fd);
}

int tcp_stream_with_timeout::connect(sockaddr_in* addr)
{
    assert(d);
    const int fd = d->socket.get().value;
    if( ::connect(fd, reinterpret_cast<struct sockaddr *>(addr), sizeof(*addr)) < 0 ) {
        if( errno != EINPROGRESS )
            return errno;
        int err = wait_for_socket(lbu::poll::poll_fd(lbu::fd(fd), lbu::poll::FlagsWriteReady));
        if( err != lbu::poll::StatusNoError )
            return err;
        socklen_t len = sizeof(err);
        getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &len);
        if( err != 0 )
            return err;
    }

    int one = 1;
    setsockopt(fd, SOL_TCP, TCP_NODELAY, &one, sizeof(one));

    return 0;
}

int tcp_stream_with_timeout::descriptor()
{
    assert(d);
    return d->socket.get().value;
}

lbu::stream::abstract_input_stream* tcp_stream_with_timeout::in()
{
    assert(d);
    return &d->in;
}

lbu::stream::abstract_output_stream* tcp_stream_with_timeout::out()
{
    assert(d);
    return &d->out;
}
