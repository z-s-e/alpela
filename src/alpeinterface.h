/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ALPEINTERFACE_H
#define ALPEINTERFACE_H

#include <alpe_thread.h>
#include <string>


class MainWindow;

class AlpeInterface final : public alpe_thread::interface, public alpe::debug_logger {
public:
    struct Settings {
        std::string output;
        bool overrideParams = false;
        int bufferTime = 3000;
        int preUnderrunTime = 60;
        int rewindSafeguard = 30;
        int emergencyPauseFade = 10;
    };

    explicit AlpeInterface(MainWindow* window);
    ~AlpeInterface();

    void setFilePath(std::string path);
    void setSettings(Settings s);

    std::string_view filePath();
    Settings settings();

    void injectIODelay();

    reset_data reset() override;
    void request_buffer(lbu::array_ref<void>) override;
    buffer_ready_info buffer_ready() override;
    alpe::long_duration_frames seek(alpe::long_duration_frames) override;
    void report_underrun(alpe::playback_underrun_event) override;
    void report_error(alpe::error_event) override;
    void log(alpe::LogLevel level, const char* message) override;

private:
    struct Data;
    Data* d = {};
};

#endif // ALPEINTERFACE_H
