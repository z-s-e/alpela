#ifndef WAVEFORMPREVIEWGENERATOR_H
#define WAVEFORMPREVIEWGENERATOR_H

#include "waveformpreview.h"

#include <QObject>

class WaveformPreviewGenerator : public QObject {
    Q_OBJECT
public:
    enum Result {
        ResultOk,
        ResultCanceled,
        ResultError
    };
    Q_ENUM(Result)

    explicit WaveformPreviewGenerator(QObject *parent = nullptr);
    ~WaveformPreviewGenerator();

    const WaveformPreview* data() { return &preview; }

    void requestPreview(const QString& file);
    void cancelPreview();

signals:
    void progress(const QString& file, unsigned endSample);
    void finished(const QString& file, WaveformPreviewGenerator::Result result);

private:
    struct Data;
    Data* d = {};

    WaveformPreview preview = {};
};

#endif // WAVEFORMPREVIEWGENERATOR_H
