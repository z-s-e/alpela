#ifndef TCP_STREAM_WITH_TIMEOUT_H
#define TCP_STREAM_WITH_TIMEOUT_H

namespace lbu::stream {
    class abstract_input_stream;
    class abstract_output_stream;
}
struct sockaddr_in;

class tcp_stream_with_timeout {
public:
    tcp_stream_with_timeout();
    ~tcp_stream_with_timeout();

    void reset();
    int connect(sockaddr_in* addr);

    int descriptor();
    lbu::stream::abstract_input_stream* in();
    lbu::stream::abstract_output_stream* out();

private:
    struct data;
    data* d = {};
};

#endif // TCP_STREAM_WITH_TIMEOUT_H
