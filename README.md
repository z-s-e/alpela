# Alpela audio player

An example / tech-demo audio player for my [Advanced Linux Playback Engine](https://gitlab.com/z-s-e/alpe).

Not that much effort went into the UI, but here is a screenshot anyway:

![(Screenshot)](screenshot.png "Screenshot")


## Installing

Currently the app is packaged only for [Arch AUR](https://aur.archlinux.org/packages/alpela). For other systems you need to build it yourself.


## Dependencies and building

Required packages are alsa, ffmpeg, libpipewire-0.3 and Qt6 (Widgets and DBus), as well as git and cmake for fetching and building. Everything else is bundled as a git submodule.

For a local user install I use the following steps (for that make sure your `PATH` env contains `$HOME/.local/bin`):

```
git clone https://gitlab.com/z-s-e/alpela.git
cd alpela
git submodule update --init
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$HOME/.local" -B build -S .
cmake --build build --target install/strip
```


## Local network lossless music streaming with minimal setup

Short instructions to stream audio over the network (somewhat similar to AirPlay):

- Run [Mapla](https://gitlab.com/z-s-e/mapla) ([AUR](https://aur.archlinux.org/packages/mapla)) on the receiving system. It will print out on which port it is listening on (default 19671 when unprivileged).
```
$ Mapla
Listening on port 19671
```

- Run Alpela on the sending system. Click "Settings", enter the IP of the receiving system and port as Output device (e.g. 192.168.0.5:19671), click OK.

- Open the audio file to play (with ⏏ or drag'n'drop).


## Known issues

The player does not work correctly with the PulseAudio Alsa plugin, as mentioned in the alpe README. On such systems you can configure the player to use an Alsa hardware output instead, of course with the drawback of the device being exclusively reserved for the player.
